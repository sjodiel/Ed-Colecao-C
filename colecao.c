#define _COLECAO_C
#include <stdio.h>
#include <stdlib.h>
#include "colecao.h"


Col *colCreate(int n){
    //Col *gc;
    if(n>0){
        Col *c = (Col*)malloc(sizeof(Col));
        if(c!=NULL){
            c->elms=(void**)malloc(sizeof(void*)*n);
            if(c->elms!=NULL){
                c->tam=n;
                c->nelms=0;
                return c;
            }else{
                free(c);
            }
        }
    }
    return NULL;
}


int colInsert(Col *c, void *elms){
    if(c!=NULL){
        if(c->nelms<c->tam){
            c->elms[c->nelms]=elms;
            c->nelms++;
            return TRUE;
        }
    }
    return FALSE;
}

void *colQuery(Col *c, void *key, int (*cmp)(void *, void*)){
    int i, achei;
    void *elm;
    if(c!=NULL){
        if(c->nelms > 0){
            i=0;
            achei=FALSE;
            while(achei!=TRUE && i<c->nelms){
                elm=c->elms[i];
                achei=cmp(elm,key);
                i++;
            }
            if(achei==TRUE){
                i--;
                return elm;
            }
        }
    }
    //return -1;
    return FALSE;
}


/*void* gcolQuery(GCol* gc, void* key, int(*cmp)(void*, void*)){
   int i=0;

   if(gc != NULL){
      if(gc->nelms > 0){
         while((cmp(key, gc->elms[i]) != TRUE) && (i < (gc->nelms))){
             i++;
         }
         if((cmp(key,gc->elms[i])) == TRUE){
            return gc->elms[i];
         }
         else return FALSE;
      }
   } return NULL;
}*/

void *colRemove(Col *c, void *key, int(*cmp)(void *, void *)){
    int i, j, achei;
    void *elm;
    if(c!=NULL){
        if(c->nelms > 0){
            i=-1;
            achei=FALSE;
            while(achei!=TRUE && i<c->nelms-1){
                i++;
                elm=c->elms[i];
                achei=cmp(elm, key);
            }
            if(achei==TRUE){
                for(j=i;j<c->nelms-1;j++){
                    c->elms[j] = c->elms[j+1];
                }
                c->nelms--;
                return elm;
            }
        }
    }
    return NULL;
}



int gcolDestroy(Col *c){
    if(c!=NULL){
        if(c->elms != NULL){
            free(c->elms);
            free(c);
            return TRUE;
        }
    }
    return FALSE;
}

