//Este programa contém a função de criar uma coleção TAD
//Jodiel Fabricio

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "colecaog.h"
#define TAMANHONOME 30

typedef struct _pessoa_{
      char nome[TAMANHONOME];
      int cpf;
      float sal;
}Pessoa;

GCol *gc = NULL;

int comparaCpf(void *a, void *b)
{
    Pessoa *pa;
    int *pb;
    pa=(Pessoa*)a;
    pb = (int*)b;
    if(*pb == pa -> cpf){
        return TRUE;
    }else{
        return FALSE;
    }
}

int comparaSal(void *pa, void *pb)
{
    Pessoa *a;
    float *b;
    a=(Pessoa*)pa;
    b=(float*)pb;
    if(a->sal==*b){
        return TRUE;
    }else{
        return FALSE;
    }
}

int comparaNome(char *sa, char *sb)
{
    return strcmp(sa,sb)==0;
}


int main(void){

        int max_elms, key;
        //long int conv;
        int i=0;
        char opcao;
        char cpf[12];
       // char subopcao =1;
       // float sal;
        Pessoa *a;

          while(1) {
            system("cls");

               puts("\n===COLECAO - GENERICA===\n");
               puts("  [1] - Criar Colecao\n");
               puts("  [2] - Inserir Elemento\n");
               puts("  [3] - Buscar Elemento\n");
               puts("  [4] - Remover Elemento\n");
               puts("  [5] - EXPLODIR\n");
               puts("  [0] - SAIR\n");
               printf(" [OPCAO]: ");
               fflush(stdin); //Limpar o Buffer do teclado
              scanf("%c", &opcao);


              switch(opcao){

                       case '1':
                           {
                                if(i<1){
                                    printf("\n\tTamanho maximo da colecao: ");
                                    scanf("%d", &max_elms); // define o tamanho a coleção
                                    i++;
                                    gc = gcolCreate(max_elms);
                                    if(gc != NULL){
                                        printf("\n\tColecao criado com sucesso!\n\n");
                                        system("pause");
                                    }
                                }else{
                                    printf("\n\tImpossivel, Destrua atual colecao!\n\n");
                                    system("pause");
                                }
                                break;
                           }
                        case '2':
                           {
                               a=(Pessoa*)malloc(sizeof(Pessoa));

                               if(gc!=NULL){
                                       // a=(Pessoa*)malloc(sizeof(Pessoa));

                                        if(gcolInsert(gc,a)){

                                            printf(" Nome: ");
                                            scanf(" %[^\n]s", a->nome);
                                            printf(" CPF: ");
                                            fflush(stdin);
                                            a->cpf = atoi(cpf);
                                            printf(" Salario: ");
                                            scanf("%f", &(a->sal));
                                            printf("\n\tElemento inserido com sucesso!\n\n");
                                            //col[i-1] = NULL;
                                             //i++;
                                             system("pause");
                                        }else{
                                            printf("\n\tErro na insercao!!\n\n");
                                            system("pause");
                                        }
                                }else{
                                    printf("\n\tNao existe Colecao.\n");
                                    system("pause");
                                }

                            break;
                           }
                        case '3':
                           {
                                if(gc!=NULL ){

                                       //system("cls");
                                       puts("\n\===COMO DESEJA PESQUISA?===\n");
                                       puts("\t[1] - Pelo nome?\n");
                                       puts("\t[2] - Pelo CPF?\n");
                                       puts("\t[3] - Pelo Salario?\n");
                                       //puts("\t0 Retornar Menue Principal:");
                                       printf("\n[ESCOLHA]: ");
                                        fflush(stdin);

                                           scanf(" %c", &opcao);
                                         //fflush(stdin); //Limpar o Buffer do teclado
                                           switch (opcao){
                                                    case '1':
                                                        {
                                                            printf("\n\tDigite o Nome: ");

                                                            scanf(" %[^\n]s", &key);

                                                            a =(Pessoa*)gcolQuery(gc, (void*)&key, &comparaNome);
                                                            if(a!=NULL){

                                                                printf("\n\t[NOME]: %s \n\t[CPF]: %u \n\t[SALARIO]: %3.f\n\n", a->nome, a->cpf, a->sal);
                                                                system("pause");
                                                                //continue;

                                                            }else{
                                                                printf("\n\tNome Incorreto!\n");
                                                                system("pause");
                                                                //continue;
                                                            }
                                                            break;
                                                        }

                                                    case '2':
                                                        {
                                                            printf("\n\tDigite o CPF: ");
                                                            scanf("%d",&key);
                                                            a =(Pessoa*)gcolQuery(gc,(void*)&key, &comparaCpf);
                                                            if (a!=NULL){
                                                                printf("\n\t[NOME]: %s \n\t[CPF]: %u \n\t[SALARIO]: %3.f\n\n", a->nome, a->cpf, a->sal);
                                                                system("pause");
                                                            }else{
                                                                printf("\n\tCpf nao existe\n");
                                                                system("pause");
                                                            }
                                                            break;
                                                        }
                                                    case '3':
                                                        {
                                                            printf("\n\tDigite o Salario: ");
                                                            scanf("%f", &key);
                                                            a=(Pessoa*)gcolQuery(gc, (void*)&key, &comparaSal);
                                                            if (a!=NULL){
                                                                printf("\n\t[NOME]: %s \n\t[CPF]: %u \n\t[SALARIO]: %3.f\n\n", a->nome, a->cpf, a->sal);
                                                                system("pause");
                                                            }else{
                                                                printf("\n\tSalario nao existe\n");
                                                                system("pause");
                                                            }



                                                            break;
                                                        }
                                                    //case '3':{ printf("SALARIO"); break;}
                                                    //case '0':{ main();  break;}
                                                    default :
                                                        {
                                                            puts("Opçao INVALIDA!!!");
                                                            system("pause");
                                                            continue;
                                                        }
                                            }

                                 }else{
                                    printf("\n\tNão existe colecao\n");
                                    system("pause");
                                }
                                break;
                           }

                        case '4':
                            {
                                if(gc!=NULL){
                                        //system("cls");

                                        puts("\n===COMO DESEJA REMOVER?===\n");
                                        puts("\t[1] - Pelo Nome?\n");
                                        puts("\t[2] - Pelo CPF?\n");
                                        puts("\t[3] - Pelo Salario?\n");
                                        //puts("\t0 Retornar Menue Principal:");
                                        printf("\n[ESCOLHA]: ");
                                            fflush(stdin); //Limpar o Buffer do teclado
                                            scanf(" %c", &opcao);
                                            //fflush(stdin); //Limpar o Buffer do teclado

                                            switch (opcao){
                                                    case '1':
                                                        {
                                                            printf("\n\tDigite o Nome: ");
                                                            scanf(" %[^\n]s", &key);
                                                            a =(Pessoa*)gcolRemove(gc, a->nome, &comparaNome);
                                                            if(a!=NULL){
                                                                    printf("\n\t[NOME]: %s \n\t[CPF]: %u \n\t[SALARIO]: %3.f\n\n", a->nome, a->cpf, a->sal);
                                                                    printf("\n REMOVIDO COM SUCESSO\n");
                                                                    //a=NULL;
                                                                    system("pause");
                                                                    //continue;

                                                            }else{
                                                                printf("\n\tNome Incorreto!\n");
                                                                system("pause");
                                                                //continue;
                                                            }
                                                            break;
                                                        }

                                                    case '2':
                                                        {
                                                            printf("\n\tDigite o CPF: ");
                                                            scanf("%d",&key);
                                                            a =(Pessoa*)gcolRemove(gc,(void*)&key, &comparaCpf);
                                                            if (a!=NULL){
                                                                printf("\n\t[NOME]: %s \n\t[CPF]: %u \n\t[SALARIO]: %3.f\n\n", a->nome, a->cpf, a->sal);
                                                                printf("\n REMOVIDO COM SUCESSO\n");
                                                                //a=NULL;
                                                                system("pause");
                                                            }else{
                                                                printf("\n\tCpf nao existe\n");
                                                                system("pause");
                                                            }
                                                            break;
                                                        }

                                                    case '3':
                                                        {
                                                            printf("\n\tPelo Salario: ");
                                                            scanf("%f", &key);
                                                            a=(Pessoa*)gcolRemove(gc, (void*)&key, &comparaSal);
                                                            if (a!=NULL){
                                                                printf("\n\t[NOME]: %s \n\t[CPF]: %u \n\t[SALARIO]: %3.f\n\n", a->nome, a->cpf, a->sal);
                                                                printf("\n REMOVIDO COM SUCESSO\n");
                                                                //a=NULL;
                                                                system("pause");
                                                            }else{
                                                                printf("\n\tSalario nao existe\n");
                                                                system("pause");
                                                            }



                                                            break;
                                                        }

                                                    //case '0':{ main();  break;}
                                                    default :
                                                        {
                                                            puts("Opçao INVALIDA!!!");
                                                            system("pause");
                                                            continue;
                                                        }
                                            }
                                }else{
                                    printf("\n\tNão existe colecao\n");
                                    system("pause");
                                }
                                break;
                            }
                        case '5':
                            {
                                if(gc!=NULL){
                                    if(gcolDestroy(gc)){
                                        printf("\n\tColecao destruida com sucesso!\n");
                                        gc=NULL;
                                        a=NULL;
                                        i=0;
                                        system("pause");
                                    }
                                }else{
                                        printf("\n\tColecao nao existe.\n");
                                        system("pause");
                                }

                              break;
                            }

                        case '0':
                            exit(0);

                        default:
                            puts("Opcao invalida!");
                            system("pause");
                            continue;
              } //End Switch
              //scanf(" %c", &opcao);
         }//while(opcao); //End While
} //End Main