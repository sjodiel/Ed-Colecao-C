#ifndef _COLECAO_H
#define _COLECAO_H
#define TRUE 1
#define FALSE 0

    #ifdef _COLECAO_C
        typedef struct _colecao_ {
            int nelms; //numero atual de elementos da Coleção // nElms;
            int tam; //tamanho máximo de elemetnos da Coleção // maxElms;
            void** elms; //vetor com elementos
        } Col;
        Col* colCreate(int n);
        int colInsert(Col* c, void* elms);
        void *colRemove(Col *c, void *key, int(*cmp)(void *, void *));
        void *colQuery(Col *c, void *key, int(*cmp)(void *, void*));
        int colDestroy(Col *c);

    #else
        typedef struct _colecao_ Col;
        extern Col* colCreate(int n);
        extern int colInsert(Col* c, void* elms);
        extern void *colRemove(Col *c, void *key, int(*cmp)(void *, void *));
        extern void *colQuery(Col *c, void *key, int(*cmp)(void *, void*));
        extern int colDestroy(Col *c);

    #endif // _COLECAO_C

#endif // _COLECAO_H
